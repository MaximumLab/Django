from django.contrib import admin

from .models import Master
from .models import Client

admin.site.register(Master)
admin.site.register(Client)
