from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.postgres.fields import JSONField


from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_client = models.BooleanField(default=False)
    is_master = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

# class Role(models.Model):
#
#   CLIENT = 1
#   MASTER = 2
#   ADMIN = 3
#   ROLE_CHOICES = (
#       (CLIENT, 'client'),
#       (MASTER, 'master'),
#       (ADMIN, 'admin'),
#   )
#
#   id = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, primary_key=True)
#
#   def __str__(self):
#       return self.get_id_display()
#
#
# class User(AbstractUser):
#   roles = models.ManyToManyField(Role)


class Client(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    phone = models.CharField(null=True, unique=True, max_length=20)


class Master(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    phone = models.CharField(null=True, unique=True, max_length=20)
    other_contacts = models.TextField()


class Comments(models.Model):
    user = models.ForeignKey(Client, on_delete=models.CASCADE)
    comment_text = models.CharField(null=False, max_length=200)
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)
    rate = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(5.0)])
    pub_date = models.DateTimeField('date published')


class Places(models.Model):
    user = models.ForeignKey(Master, on_delete=models.CASCADE)
    lon = models.FloatField()
    lat = models.FloatField()
    start_at = models.TimeField()
    stop_at = models.TimeField()
    working_days = models.JSONField
    price = models.IntegerField()
    seo_title = models.CharField(null=True, max_length=100)
    seo_description = models.CharField(null=True, max_length=200)
    seo_keywords = models.CharField(null=True, max_length=200)
    pub_date = models.DateTimeField('date published')
