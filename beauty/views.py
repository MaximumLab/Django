from django.http import HttpResponse


def index(request):
    return HttpResponse("Hello, world. You're at the beauty index.")

def detail(request, master_id):
    return HttpResponse("You're looking at master %s." % master_id)