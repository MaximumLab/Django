### db
```
docker-compose exec db bash
psql -U postgres
\dt
\d table_name
https://stackoverflow.com/questions/3414247/how-to-drop-all-tables-from-the-database-with-manage-py-cli-in-django
```

### web
```
docker-compose exec web bash
python manage.py makemigrations beauty
python3 -m django --version
```